��    /      �  C           >     
   X     c     g     o     }     �     �     �     �     �     �     �     �     �     �     �  /   �     "     ;     O     U  
   ^     i     n     v     z     }     �     �     �     �     �  	   �     �     �     �  	   �     �                          $     3     C  �  X  _   �  	   I     S  	   _     i     �     �     �     �  	   �     �     �     �     �  	   �     �     	  X   	  #   q	     �	     �	  	   �	  
   �	     �	     �	  	   �	     �	     �	     �	     �	  %   �	     
     :
     @
  	   G
     Q
     X
  	   ^
     h
     o
     w
     �
     �
     �
     �
  &   �
                           '       "                       #   /          &          $           (          +          *                          
       -   !             %            	                                 .      ,   )           A plugin, to create and display a voteadviser on your website. Abstention Add Add New Add new party Add new question Add question All Answer Answers Approve Back Compare Decline Edit Enter answer here Enter name here Enter new question or leave blank for canceling Enter new question title Enter question here Error Evaluate Evaluation Logo Neutral New No Notice Parties Party Please add a party at first. Please choose a party Question Questions Remove Restart Search Shortcode Success View VoteAdviser Your opinion found found in Trash no answer given no party left to add Project-Id-Version: wp-voteadviser
POT-Creation-Date: 2017-02-26 19:59+0100
PO-Revision-Date: 2017-02-26 20:06+0100
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Ein Plugin, welches das Erstellen und Anbieten eines "Wahl-O-Maten" (VoteAdvisers) ermöglicht. Enthalten Hinzufügen Erstellen Neue Partei hinzufügen Neue Frage hinzufügen Frage hinzufügen Alle Antwort Antworten Dafür Zurück Vergleichen Dagegen Bearbeite Antwort hier eingeben Namen hier eingeben Füge eine neue Fragen hinzu oder lasse das Feld leer, um keine neue Frage hinzuzufügen Füge einen neuen Fragentitel hinzu Frage hier eingeben Fehler Auswerten Auswertung Logo Neutral Erstellen Keine Info Parteien Partei Bitte füge zuerst eine Partei hinzu. Bitte eine Partei auswählen Frage Fragen Entfernen Erneut Suche Shortcode Erfolg Ansehen VoteAdviser Deine Meinung gefunden im Papierkorb gefunden keine Antwort hinterlegt keine Partei zum Hinzufügen vorhanden 