<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_Base')) {

    abstract class WPVA_Base {

        /**
         * fires actions, filters and shortcodes
         */
        public function __construct() {
            $this->handle_actions();
            $this->handle_filters();
            $this->handle_shortcodes();
        }

        /** following methods may be overwritten by subclasses */

        public function activate() {
            // placeholder method
        }

        public function deactivate() {
            // placeholder method
        }

        protected function handle_actions() {
            // placeholder method
        }

        protected function handle_filters() {
            // placeholder method
        }

        protected function handle_shortcodes() {
            // placeholder method
        }
    }
}