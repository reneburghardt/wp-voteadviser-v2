<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_PostType_Answer')) {

    class WPVA_PostType_Answer extends WPVA_PostType {

        const SLUG = 'wpva-answer';

        protected static function get_post_type_params()
        {
            $params =  parent::get_post_type_params();
            $params['show_ui'] = false;
            $params['supports'][] = 'editor';
            return $params;
        }

        public static function get_title_text() {
            return __('Enter answer here', 'wp-voteadviser');
        }

        /**
         * @return string
         */
        public static function get_name() {
            return __('Answer', 'wp-voteadviser');
        }

        /**
         * @return string
         */
        public static function get_plural() {
            return __('Answers', 'wp-voteadviser');
        }

        /**
         * @return string
         */
        public static function get_rest_base() {
            return 'wpva-answers';
        }
    }
}