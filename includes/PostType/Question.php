<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_PostType_Question')) {

    class WPVA_PostType_Question extends WPVA_PostType {

        const SLUG = 'wpva-question';

        protected static function get_post_type_params() {
            $params =  parent::get_post_type_params();
            $params['show_ui'] = false;
            $params['supports'][] = 'editor';
            return $params;
        }

        public static function get_title_text() {
            return __('Enter question here', 'wp-voteadviser');
        }

        /**
         * @return string
         */
        public static function get_name() {
            return __('Question', 'wp-voteadviser');
        }

        /**
         * @return string
         */
        public static function get_plural() {
            return __('Questions', 'wp-voteadviser');
        }

        /**
         * @return string
         */
        public static function get_rest_base() {
            return 'wpva-questions';
        }
    }
}