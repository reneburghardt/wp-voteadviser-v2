<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_PostType_Party')) {

    class WPVA_PostType_Party extends WPVA_PostType {

        const SLUG = 'wpva-party';

        protected function handle_actions() {
            parent::handle_actions();

            add_action('init', array('WPVA_PostType_Party', 'add_new_featured_image_size'));
            add_action('admin_head', array('WPVA_PostType_Party', 'change_featured_image_meta_box_title'));
            add_action('admin_enqueue_scripts', array('WPVA_PostType_Party', 'add_voteadviser_party_admin_style'));
        }

        protected function handle_filters() {
            parent::handle_filters();

            add_filter('image_size_names_choose', array('WPVA_PostType_Party', 'add_new_featured_image_size_name'));
        }

        public static function get_title_text() {
            return __('Enter name here', 'wp-voteadviser');
        }


        protected static function get_post_type_params() {
            $params =  parent::get_post_type_params();
            $params['supports'][] = 'thumbnail';
            return $params;
        }

        public static function change_featured_image_meta_box_title() {
            remove_meta_box('postimagediv', self::SLUG, 'side');
            add_meta_box('postimagediv', 'Logo', 'post_thumbnail_meta_box', self::SLUG, 'normal', 'high');
        }

        public static function add_new_featured_image_size() {
            add_image_size('party-logo', 300, 300, array('center', 'center'));
        }

        public static function add_voteadviser_party_admin_style() {
            global $post_type;
            if (((isset($_GET['post_type']) && $_GET['post_type'] === WPVA_DataType_Party::SLUG)
                || $post_type === WPVA_DataType_Party::SLUG)) {
                wp_register_style('wpva_admin_voteadviser_party_style', plugins_url(PLUGIN_SLUG . '/res/css/admin/wp-voteadviser-party.css'));
                wp_enqueue_style('wpva_admin_voteadviser_party_style');
            }
        }

        /**
         * @param array $sizes
         * @return array
         */
        public static function add_new_featured_image_size_name(array $sizes) {
            return array_merge( $sizes, array(
                'party-logo' => __('Logo'),
            ) );
        }

        /**
         * @return string
         */
        public static function get_name() {
            return __('Party', 'wp-voteadviser');
        }

        /**
         * @return string
         */
        public static function get_plural() {
            return __('Parties', 'wp-voteadviser');
        }

        /**
         * @return string
         */
        public static function get_rest_base() {
            return 'wpva-parties';
        }
    }
}