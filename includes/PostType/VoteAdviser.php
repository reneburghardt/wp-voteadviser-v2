<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_PostType_VoteAdviser')) {

    class WPVA_PostType_VoteAdviser extends WPVA_PostType {

        protected function handle_actions() {
            parent::handle_actions();

            $slug = WPVA_DataType_VotingAdviser::SLUG;
            add_action('add_meta_boxes_' . $slug, array('WPVA_PostType_VoteAdviser', 'add_voteadviser_main_meta_box'));
            add_action('add_meta_boxes_' . $slug, array('WPVA_PostType_VoteAdviser', 'add_voteadviser_parties_meta_box'));
            add_action('add_meta_boxes_' . $slug, array('WPVA_PostType_VoteAdviser', 'add_voteadviser_shortcode_meta_box'));
            add_action('save_post_' . $slug, array('WPVA_PostType_VoteAdviser', 'save_voteadviser'));
            add_action('save_post_' . $slug, array('WPVA_PostType_VoteAdviser', 'save_voteadviser_main_meta_box'));
            add_action('save_post_' . $slug, array('WPVA_PostType_VoteAdviser', 'save_voteadviser_parties_meta_box'));
            add_action('admin_enqueue_scripts', array('WPVA_PostType_VoteAdviser', 'add_voteadviser_admin_style'));
            add_action('admin_enqueue_scripts', array('WPVA_PostType_VoteAdviser', 'add_voteadviser_admin_script'));

            // rest api
            add_action('rest_api_init', function() {
                register_rest_route('wp/v2', '/wpva-voteadviser/(?P<id>\d+)/questions', array(
                        'methods' => WP_REST_Server::READABLE,
                        'callback' => array('WPVA_PostType_VoteAdviser', 'get_questions_rest')
                    )
                );
            });
            add_action('rest_api_init', function() {
                register_rest_route('wp/v2', '/wpva-voteadviser/(?P<id>\d+)/parties', array(
                        'methods' => WP_REST_Server::READABLE,
                        'callback' => array('WPVA_PostType_VoteAdviser', 'get_parties_rest')
                    )
                );
            });
        }

        public static function get_slug() {
            return WPVA_DataType_VotingAdviser::SLUG;
        }

        protected static function get_post_type_params() {
            $params =  parent::get_post_type_params();
            $params['public'] = true;
            return $params;
        }

        //TODO: consolidate meta box handling?
        public static function add_voteadviser_main_meta_box(
            /** @noinspection PhpUnusedParameterInspection */ $post
        ) {
            add_meta_box(
                'voteadviser_main_box',
                __('VoteAdviser', 'wp-voteadviser'),
                array('WPVA_PostType_VoteAdviser', 'render_voteadviser_main_meta_box'),
                WPVA_DataType_VotingAdviser::SLUG,
                'normal',
                'high'
            );
        }

        public static function add_voteadviser_parties_meta_box(
            /** @noinspection PhpUnusedParameterInspection */ $post
        ) {
            add_meta_box(
                'voteadviser_parties_box',
                __('Parties', 'wp-voteadviser'),
                array('WPVA_PostType_VoteAdviser', 'render_voteadviser_parties_meta_box'),
                WPVA_DataType_VotingAdviser::SLUG,
                'normal',
                'default'
            );
        }

        public static function add_voteadviser_shortcode_meta_box(
            /** @noinspection PhpUnusedParameterInspection */ $post
        ) {
            add_meta_box(
                'voteadviser_shortcode_box',
                __('Shortcode', 'wp-voteadviser'),
                array('WPVA_PostType_VoteAdviser', 'render_voteadviser_shortcode_meta_box'),
                WPVA_DataType_VotingAdviser::SLUG,
                'side',
                'default'
            );
        }

        //TODO: unnecessary?
        public static function save_voteadviser($post_id) {
            if (!isset($_POST['voteadviser_main_meta_box_nonce'])
                || !wp_verify_nonce($_POST['voteadviser_main_meta_box_nonce'], basename(__FILE__))
                || !current_user_can('edit_post', $post_id)
            ) {
                return;
            }
            // hacky, but common solution for avoiding infinite loop of wp_update_post and save_post_{$post->post_type}
            $slug = WPVA_DataType_VotingAdviser::SLUG;
            remove_action('save_post_' . $slug, array('WPVA_PostType_VoteAdviser', 'save_voteadviser'));
            $voting_adviser = new WPVA_DataType_VotingAdviser();
            $voting_adviser->fill_by_id($post_id)->save();
//            wp_update_post(array(
//                'ID' => $post_id,
//                'post_content' => "[wp_voteadviser ". "id='{$post_id}' title=''" ."]",
//            ));
            add_action('save_post_' . $slug, array('WPVA_PostType_VoteAdviser', 'save_voteadviser'));
        }

        public static function save_voteadviser_main_meta_box($post_id) {
            if (!isset($_POST['voteadviser_main_meta_box_nonce'])
                || !wp_verify_nonce($_POST['voteadviser_main_meta_box_nonce'], basename(__FILE__))
                || !current_user_can('edit_post', $post_id)
            ) {
                return;
            }
            $new_question_id = -1;
            if (isset($_REQUEST['question_new']) && $_REQUEST['question_new'] !== '') {
                $question = new WPVA_DataType_Question();
                $new_question_id = $question->set_title(sanitize_text_field($_REQUEST['question_title_new']))
                    ->set_content(sanitize_text_field($_REQUEST['question_new']))
                    ->insert();
                $old_party_ids_as_string = sanitize_text_field($_POST['_wpva_old_party_ids']);
                $old_party_ids = !empty($old_party_ids_as_string) ? explode(',', $old_party_ids_as_string) : array();
                foreach ($old_party_ids as $party_id) {
                    $answer = isset($_POST['answers']['new'][$party_id])
                        ? sanitize_textarea_field($_POST['answers']['new'][$party_id])
                        : '';
                    $opinion = isset($_POST['opinions']['new'][$party_id])
                        ? sanitize_text_field($_POST['opinions']['new'][$party_id])
                        : 0;

                    $answer_object = new WPVA_DataType_Answer();
                    $answer_object->set_content($answer);
                    $answer_object->set_question($new_question_id);
                    $answer_object->set_opinion($opinion);
                    $answer_object->set_party($party_id);
                    $answer_object->save();
                }
            }
            if (isset($_REQUEST['_wpva_old_question_ids'])) {
                $question_ids = sanitize_text_field($_POST['_wpva_old_question_ids']);
                if (isset($_REQUEST['wpva_question_remove'])) {
                    $remove_id = sanitize_text_field($_POST['wpva_question_remove']);
                    $question_ids_list = explode(',', $question_ids);
                    $question_ids = implode(',', array_diff($question_ids_list, array($remove_id)));
                }
                if (isset($_REQUEST['wpva_question_up'])) {
                    $up_id = sanitize_text_field($_POST['wpva_question_up']);
                    $question_ids_list = explode(',', $question_ids);
                    $position = array_search($up_id, $question_ids_list);
                    if ($position) {
                        $question_ids_list[$position] = $question_ids_list[$position-1];
                        $question_ids_list[$position-1] = $up_id;
                    }
                    $question_ids = implode(',', $question_ids_list);
                }
                if ($new_question_id > 0) {
                    $question_ids .= $question_ids != '' ? ',' : '';
                    $question_ids .= $new_question_id;
                }
                update_post_meta($post_id, '_wpva_question_ids', $question_ids);
            }
            if (isset($_REQUEST['_wpva_old_question_ids']) && isset($_REQUEST['_wpva_old_party_ids'])) {
                $old_question_ids_as_string = sanitize_text_field($_POST['_wpva_old_question_ids']);
                $old_question_ids = !empty($old_question_ids_as_string) ? explode(',', $old_question_ids_as_string) : array();
                $old_party_ids_as_string = sanitize_text_field($_POST['_wpva_old_party_ids']);
                $old_party_ids = !empty($old_party_ids_as_string) ? explode(',', $old_party_ids_as_string) : array();
                foreach ($old_question_ids as $old_question_id) {
                    foreach ($old_party_ids as $old_party_id) {
                        $answer = isset($_POST['answers'][$old_question_id][$old_party_id])
                            ? sanitize_textarea_field($_POST['answers'][$old_question_id][$old_party_id])
                            : '';
                        $opinion = isset($_POST['opinions'][$old_question_id][$old_party_id])
                            ? sanitize_text_field($_POST['opinions'][$old_question_id][$old_party_id])
                            : 0;

                        $answer_object = new WPVA_DataType_Answer();
                        $answer_object->fill_by_id(WPVA_DataType_Answer::get_id_by_question_and_party(
                            $old_question_id, $old_party_id
                        ));
                        $answer_object->set_content($answer)
                            ->set_opinion($opinion)
                            ->save();
                    }
                }
            }
        }

        public static function save_voteadviser_parties_meta_box($post_id) {
            if (!isset($_POST['voteadviser_parties_meta_box_nonce'])
                || !wp_verify_nonce($_POST['voteadviser_parties_meta_box_nonce'], basename(__FILE__))
                || !current_user_can('edit_post', $post_id)
                || !isset($_REQUEST['_wpva_party_ids'])
            ) {
                return;
            }
            $_wpva_party_ids = implode(',', array_diff($_POST['_wpva_party_ids'], array(-1)));
            update_post_meta($post_id, '_wpva_party_ids', $_wpva_party_ids);
        }

        public static function render_voteadviser_main_meta_box($post) {
            wp_nonce_field( basename( __FILE__ ), 'voteadviser_main_meta_box_nonce' );
            $party_ids_as_string = wpva_get_post_meta($post->ID, '', '_wpva_party_ids', true);
            $party_ids = $party_ids_as_string !== '' ? explode(',', $party_ids_as_string) : array();
            $question_ids_as_string = wpva_get_post_meta($post->ID, '', '_wpva_question_ids', true);
            $question_ids = $question_ids_as_string !== '' ? explode(',', $question_ids_as_string) : array();
            wpva_render_template('admin/main_meta_box.php', array(
                'question_ids' => $question_ids_as_string,
                'questions' => WPVA_DataType_Question::get_questions(
                    $question_ids
                ),
                'party_ids' => $party_ids_as_string,
                'parties' => WPVA_DataType_Party::get_parties(
                    $party_ids
                ),
                'answers' => WPVA_DataType_Answer::get_answers(
                    $question_ids,
                    $party_ids
                )
            ), true);
        }

        public static function render_voteadviser_parties_meta_box($post) {
            wp_nonce_field( basename( __FILE__ ), 'voteadviser_parties_meta_box_nonce' );
            $party_ids_as_string = wpva_get_post_meta($post->ID, '', '_wpva_party_ids', true);
            $party_ids = $party_ids_as_string !== '' ? explode(',', $party_ids_as_string) : array();
            wpva_render_template('admin/parties_meta_box.php', array(
                'party_ids' => $party_ids_as_string,
                'parties' => WPVA_DataType_Party::get_parties(
                    $party_ids
                ),
                'all_parties' => get_posts(array(
                    'exclude' => $party_ids_as_string,
                    'post_type' => WPVA_PostType_Party::get_slug(),
                    'post_status' => 'publish'
                ))
            ), true);
        }


        public static function render_voteadviser_shortcode_meta_box($post) {
            wpva_render_template('admin/shortcode_meta_box.php', array(
                'post_id' => $post->ID,
            ), true);
        }

        public static function add_voteadviser_admin_style($hook) {
            if ($hook === 'post.php') {
                wp_register_style('wpva_admin_voteadviser_style', plugins_url(PLUGIN_SLUG . '/res/css/admin/wp-voteadviser.css'));
                wp_enqueue_style('wpva_admin_voteadviser_style');
            }
        }


        public static function add_voteadviser_admin_script($hook) {
            if ($hook === 'post.php') {
                wp_enqueue_script(
                    'wpva_admin_voteadviser_script',
                    plugins_url(PLUGIN_SLUG . '/res/js/admin/wp-voteadviser.js'),
                    array('jquery'),
                    false,
                    true
                );
            }
        }

        public static function get_questions_rest($request) {
            // TODO: error handling no id
            return new WP_REST_Response(
                WPVA_DataType_VotingAdviser::get_questions($request['id'])
            );
        }

        public static function get_parties_rest($request) {
            // TODO: error handling no id
            return new WP_REST_Response(
                WPVA_DataType_VotingAdviser::get_parties($request['id'])
            );
        }

        /**
         * @return string
         */
        public static function get_name()
        {
            return __('VoteAdviser', 'wp-voteadviser');
        }

        /**
         * @return string
         */
        public static function get_rest_base() {
            return 'wpva-voteadviser';
        }
    }
}