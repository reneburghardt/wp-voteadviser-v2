<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_Shortcode')) {

    class WPVA_Shortcode extends WPVA_Base {

        protected function handle_actions() {
            add_action('wp_enqueue_scripts', array('WPVA_Shortcode', 'add_voteadviser_shortcode_style'));
        }

        protected function handle_shortcodes() {
            parent::handle_shortcodes();

            add_shortcode('wp_voteadviser', array('WPVA_Shortcode', 'render_voteadviser_shortcode'));
        }

        public static function render_voteadviser_shortcode($attr) {
            $id = $attr['id'];
            // TODO: replace by str_replace and template file
            $content = "<div id=\"voteadviser-{$id}\" class=\"voteadviser\">";
            $content .= wpva_get_template('wpva-voteadviser.html');
            $content .= '</div>';
            echo $content;
        }

        public static function add_voteadviser_shortcode_style() {
            wp_register_style('wpva_voteadviser_shortcode_style', plugins_url(PLUGIN_SLUG . '/res/css/voteadviser-shortcode.css'));
            wp_enqueue_style('wpva_voteadviser_shortcode_style');
        }
    }
}