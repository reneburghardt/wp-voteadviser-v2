<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_Initiator')) {

    /**
     * should be called once to initiate plugin
     * TODO: singleton-mechanism?
     */
    class WPVA_Initiator extends WPVA_Base {

        private $modules = array();

        /**
         * instantiates needed objects
         */
        public function __construct() {
            parent::__construct();

            $this->modules[] = new WPVA_PostType_VoteAdviser();
            $this->modules[] = new WPVA_PostType_Party();
            $this->modules[] = new WPVA_PostType_Question();
            $this->modules[] = new WPVA_PostType_Answer();
            $this->modules[] = new WPVA_Result();
            $this->modules[] = new WPVA_Shortcode();
        }

        protected function handle_actions() {
            parent::handle_actions();

            add_action('plugins_loaded', array('WPVA_Initiator', 'load_textdomain'));
            add_action('wp_enqueue_scripts', array('WPVA_Initiator', 'add_voteadviser_vue'));
            add_action('wp_footer', array('WPVA_Initiator', 'add_voteadviser_rest_api_url'));
            add_action('wp_enqueue_scripts', array('WPVA_Initiator', 'add_voteadviser_icon_font'));
            add_action('admin_enqueue_scripts', array('WPVA_Initiator', 'add_voteadviser_icon_font'));
        }

        /**
         * calls activation routines of modules
         */
        public function activate() {
            parent::activate();

            foreach ($this->modules as $module) {
                register_activation_hook(__FILE__, array($module, 'activate'));
            }
        }

        /**
         * calls deactivation routines of modules
         */
        public function deactivate() {
            parent::deactivate();

            foreach ($this->modules as $module) {
                register_deactivation_hook(__FILE__, array($module, 'deactivate'));
            }
        }

        public static function load_textdomain() {
            load_plugin_textdomain('wp-voteadviser', false, dirname(plugin_basename(__FILE__)) . '/../lang/');
        }

        public static function add_voteadviser_icon_font() {
            wp_register_style('wpva_material_icon_font', 'https://fonts.googleapis.com/icon?family=Material+Icons');
            wp_enqueue_style('wpva_material_icon_font');
        }

        public static function add_voteadviser_vue() {
            $src = 'https://cdn.jsdelivr.net/npm/vue';
            if (defined('WP_DEBUG') && WP_DEBUG) {
                $src = 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js';
            }
            wp_register_script('wpva-vue', $src);
            wp_enqueue_script('wpva-vue');
            wp_register_script('wpva-js', plugins_url(PLUGIN_SLUG . '/res/js/wp-voteadviser.js'), array('jquery'));
            wp_enqueue_script('wpva-js');
        }

        public static function add_voteadviser_rest_api_url() {
            ?>
            <script>
                var wpRestApiUrl = '<?php echo get_rest_url(); ?>';
            </script>
            <script type="text/x-template" id="question-template">
                <?php echo wpva_get_template('wpva-question.html'); ?>
            </script>
            <?php
        }
    }
}