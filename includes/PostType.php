<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_PostType')) {

	abstract class WPVA_PostType extends WPVA_Base {

		public function activate() {
			$this->create_post_type();
		}

        public function create_post_type() {
            if (!post_type_exists(static::get_slug())) {
                register_post_type(static::get_slug(), static::get_post_type_params());
            }
        }

        /**
         * @param string $title
         * @return string
         */
        public function change_title_text($title) {
            $screen = get_current_screen();
            $post_type_title = static::get_title_text();
            if ($screen->post_type == static::get_slug() && $post_type_title !== false) {
                return $post_type_title;
            }
            return $title;
        }

        /**
         * @return string
         * TODO: make abstract
         */
        public static function get_slug() {
            /** @noinspection PhpUndefinedClassConstantInspection */
            return static::SLUG;
        }

        /**
         * @return string
         * @throws Exception
         */
        public static function get_name() {
            // TODO: no generic exception
            throw new Exception('Please define a name for your post type');
        }

        /**
         * @return string
         */
        public static function get_plural() {
            return static::get_name();
        }

        /**
         * @return string
         */
        public static function get_rest_base() {
            return static::get_name();
        }

        /**
         * @return string|bool
         */
        public static function get_title_text() {
            return false;
        }

        protected function handle_actions() {
            parent::handle_actions();

            add_action('init', array($this, 'create_post_type' ));
        }

        protected function handle_filters() {
            parent::handle_filters();

            add_filter('enter_title_here', array($this, 'change_title_text'));
        }

        /**
         * @return array
         */
        protected static function get_post_type_params() {
            $params = array(
                'show_ui'              => true,
                'supports'             => array('title'),
                'show_in_rest'         => true,
                'rest_base'            => static::get_rest_base()
            );
            $params['labels'] = static::get_post_type_labels();
			return $params;
		}

        /**
         * @return array
         */
        protected static function get_post_type_labels() {
            $singular = static::get_name();
            $plural = static::get_plural();
            return array(
                'name'               => $plural,
                'singular_name'      => $singular,
                'add_new'            => __('Add New', 'wp-voteadviser-v2'),
                'add_new_item'       => __('Add New', 'wp-voteadviser-v2') . ' ' . $singular,
                'edit'               => __('Edit', 'wp-voteadviser-v2'),
                'edit_item'          => __('Edit', 'wp-voteadviser-v2') . ' ' . $singular,
                'new_item'           => __('New', 'wp-voteadviser-v2') . ' ' . $singular,
                'view'               => __('View', 'wp-voteadviser-v2') . ' ' . $plural,
                'view_item'          => __('View', 'wp-voteadviser-v2') . ' ' . $singular,
                'all_items'          => __('All', 'wp-voteadviser-v2') . ' ' . $plural,
                'search_items'       => __('Search', 'wp-voteadviser-v2') . ' ' . $plural,
                'not_found'          => __('No', 'wp-voteadviser-v2') . ' ' . $plural . ' ' . __('found', 'wp-voteadviser-v2'),
                'not_found_in_trash' => __('No', 'wp-voteadviser-v2') . ' ' . $plural . ' ' . __('found in Trash', 'wp-voteadviser-v2'),
                'parent'             => __('Parent', 'wp-voteadviser-v2') . ' ' . $singular
            );
        }
	}
}