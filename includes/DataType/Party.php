<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_DataType_Party')) {

    class WPVA_DataType_Party extends WPVA_DataType {

        const SLUG = 'wpva-party';

        protected $ID;
        protected $title = '';
        protected $content;
        protected $logo_url;

        /**
         * @return array
         */
        public function get_data() {
            $data =  parent::get_data();
            $data['logo_url'] = $this->get_logo_url();
            return $data;
        }

        /**
         * @return string|false
         */
        public function get_logo_url() {
            if ($this->has_ID()) {
                return get_the_post_thumbnail_url($this->get_ID(), 'party-logo');
            }
            return false;
        }

        /**
         * @param array $party_ids
         * @return array
         */
        public static function get_parties(array $party_ids) {
            $parties = array();
            foreach ($party_ids as $party_id) {
                $party = new WPVA_DataType_Party();
                $parties[] = $party->fill_by_id($party_id)->get_data();
            }
            return $parties;
        }
    }
}