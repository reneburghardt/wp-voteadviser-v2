<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_DataType_VotingAdviser')) {

    class WPVA_DataType_VotingAdviser extends WPVA_DataType {

        const SLUG = 'wpva-voteadviser';

        protected $ID;
        protected $title = '';
        protected $content;

        /**
         * @return string
         */
        public function get_content() {
            if (!isset($this->content) || !$this->content) {
                $this->content = "[wp_voteadviser ". "id='{$this->get_ID()}' title=''" ."]";
            }
            return $this->content;
        }

        public static function get_parties($va_id) {
            $party_ids_as_string = wpva_get_post_meta($va_id, '', '_wpva_party_ids', true);
            $party_ids = $party_ids_as_string !== '' ? explode(',', $party_ids_as_string) : array();
            return WPVA_DataType_Party::get_parties($party_ids);
        }

        public static function get_questions($va_id) {
            $question_ids_as_string = wpva_get_post_meta($va_id, '', '_wpva_question_ids', true);
            $question_ids = $question_ids_as_string !== '' ? explode(',', $question_ids_as_string) : array();
            return WPVA_DataType_Question::get_questions($question_ids);
        }
    }
}