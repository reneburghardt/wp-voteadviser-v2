<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_DataType_Question')) {

    class WPVA_DataType_Question extends WPVA_DataType {

        const SLUG = 'wpva-question';

        protected $ID;
        protected $title = '';
        protected $content;

        /**
         * @param array $question_ids
         * @return array
         */
        public static function get_questions(array $question_ids) {
            $questions = array();
            $nr = 1;
            $count = count($question_ids);
            foreach ($question_ids as $question_id) {
                $question = new WPVA_DataType_Question();
                $questions[] = $question->fill_by_id($question_id)->get_data($nr++, $count);
            }
            return $questions;
        }

        public function get_data($nr = null, $count = null) {
            return array(
                'id'        => $this->ID,
                'title'     => $this->title,
                'question'  => $this->content,
                'post_type' => WPVA_DataType_Question::SLUG,
                'nr'        => $nr,
                'count'     => $count
            );
        }
    }
}