<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_DataType_Vote')) {

    class WPVA_DataType_Vote extends WPVA_DataType {

        const WPVA_VOTE_TABLE_NAME = 'wpva_vote';

        protected $ID;
        protected $voteadviser_id = null;
        protected $opinions = array();
        protected $favorites = array();


        public function fill_by_id($id) {
            $this->ID = trim($id);
            global $wpdb;
            $table_name = self::get_table_name();
            /** @noinspection SqlResolve */
            $vote = $wpdb->get_row("SELECT * FROM $table_name WHERE id=$this->ID");
            $this->voteadviser_id = $vote->voteadviser_id;
            $this->opinions = unserialize($vote->opinions);
            $this->favorites = unserialize($vote->favorites);
            return $this;
        }


        private static function get_table_name() {
            global $wpdb;
            return $wpdb->prefix . self::WPVA_VOTE_TABLE_NAME;
        }


        protected function prepared_data($force_id = false) {
            if ($force_id || $this->has_ID()) {
                $data['ID'] = $this->get_ID();
            }
            $data['voteadviser_id'] = $this->get_voteadviser_id();
            $data['opinions'] = serialize($this->get_opinions());
            $data['favorites'] = serialize($this->get_favorites());
            return $data;
        }


        public function insert() {
            global $wpdb;
            $table_name = self::get_table_name();
            $success = $wpdb->insert($table_name, $this->prepared_data());
            if (!$success) {
                return 0;
            }
            return $wpdb->insert_id;
        }


        public function update() {
            global $wpdb;
            $table_name = self::get_table_name();
            $data = $this->prepared_data(true);
            $success = $wpdb->update($table_name, $data, array(
                'ID' => $data['ID']
            ));
            if (!$success) {
                return 0;
            }
            return $data['ID'];
        }


        public function get_voteadviser_id() {
            return $this->voteadviser_id;
        }


        public function set_voteadviser_id($id) {
            $this->voteadviser_id = $id;
            return $this;
        }


        public function get_opinions() {
            return $this->opinions;
        }


        public function set_opinions(array $opinions) {
            $this->opinions = $opinions;
            return $this;
        }


        /**
         * @return array
         */
        public function get_favorites() {
            return $this->favorites;
        }


        /**
         * @param array $favorites
         * @return $this
         */
        public function set_favorites(array $favorites) {
            $this->favorites = $favorites;
            return $this;
        }

    }

}