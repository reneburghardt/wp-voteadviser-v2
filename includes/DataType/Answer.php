<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_DataType_Answer')) {

    class WPVA_DataType_Answer extends WPVA_DataType {

        const SLUG = 'wpva-answer';
        const WPVA_2_ANSWER_TABLE_NAME = 'wpva_2_answer';

        protected $ID;
        protected $title;
        protected $content = '';
        protected $opinion = 0;
        protected $question_id = null;
        protected $party_id = null;


        public static function get_id_by_question_and_party($question_id, $party_id) {
            $answer = new WPVA_DataType_Answer();
            $answer->set_question(trim($question_id));
            $answer->set_party(trim($party_id));
            global $wpdb;
            $table_name = self::get_table_name();
            /** @noinspection SqlResolve */
            $answer->ID = $wpdb->get_var("SELECT answer_id FROM $table_name
                                   WHERE question_id = $question_id
                                   AND party_id = $party_id");
            return $answer->get_ID();
        }


        /**
         * @param array $question_ids
         * @param array $party_ids
         * @return array
         */
        public static function get_answers(array $question_ids, array $party_ids) {
            $answers = array();
            foreach (array_filter($question_ids) as $question_id) {
                foreach (array_filter($party_ids) as $party_id) {
                    $answer = new WPVA_DataType_Answer();
                    $answer->fill_by_id(WPVA_DataType_Answer::get_id_by_question_and_party(
                        trim($question_id),
                        trim($party_id)
                    ));
                    $answers[$question_id][$party_id] = $answer->get_data();
                }
            }
            return $answers;
        }


        /**
         * @return string
         */
        private static function get_table_name() {
            global $wpdb;
            return $wpdb->prefix . self::WPVA_2_ANSWER_TABLE_NAME;
        }


        public function fill_by_id($id) {
            parent::fill_by_id($id);

            global $wpdb;
            $table_name = self::get_table_name();
            /** @noinspection SqlResolve */
            $ids = $wpdb->get_row("SELECT * FROM $table_name WHERE answer_id = $this->ID");
            $this->set_question($ids->question_id)
                 ->set_party($ids->party_id);
            $this->set_opinion(wpva_get_post_meta($this->get_ID(), 0, '_wpva_answer_opinion', true));
            return $this;
        }


        public function get_data() {
            $data = parent::get_data();
            $data['opinion'] = $this->opinion;
            return $data;
        }


        public function get_title() {
            if (!isset($this->title) || !$this->title) {
                $this->title = "Answer";
                //TODO
            }
            return $this->title;
        }


        public function insert() {
            $post_id = parent::insert();
            $this->ID = $post_id;
            global $wpdb;
            $table_name = self::get_table_name();
            $wpdb->insert($table_name, array(
                'answer_id' => $this->ID,
                'question_id' => $this->question_id,
                'party_id' => $this->party_id
            ));
            $this->update_answer_opinion();
            return $post_id;
        }


        public function update() {
            $post_id = parent::update();
            $this->update_answer_opinion();
            return $post_id;
        }


        public function get_opinion() {
            return $this->opinion;
        }


        public function set_opinion($opinion) {
            $this->opinion = $opinion;
            return $this;
        }


        public function set_question($question_id) {
            $this->question_id = $question_id;
            return $this;
        }


        public function set_party($party_id) {
            $this->party_id = $party_id;
            return $this;
        }


        protected function update_answer_opinion() {
            update_post_meta($this->get_ID(), '_wpva_answer_opinion', $this->opinion);
        }

    }

}