<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

define('PLUGIN_SLUG', 'wp-voteadviser-v2');

/**
 * custom autoloader function
 *
 * @param string $class_name
 */
function wpva_autoloader($class_name) {
    if (substr($class_name, 0, 5) === 'WPVA_') {
        $class_name = substr($class_name, 5);
        $classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR;
        $class_file = str_replace( '_', DIRECTORY_SEPARATOR, $class_name ) . '.php';
        /** @noinspection PhpIncludeInspection */
        require_once $classes_dir . $class_file;
    }
}
spl_autoload_register( 'wpva_autoloader' );

/**
 * gets meta data from a post, but fall back to
 * given default if no meta data stored in db yet
 *
 * @param int    $post_id
 * @param mixed  $default
 * @param string $key
 * @param bool   $single
 * @return mixed
 */
function wpva_get_post_meta($post_id, $default, $key = '', $single = false) {
    $post_meta = get_post_meta($post_id, $key, $single);
    if (empty($post_meta)) {
        return $default;
    }
    return $post_meta;
}

function wpva_get_template($end_path) {
    $url = plugins_url(PLUGIN_SLUG . '/templates/' . $end_path);
    // file_get_contents seems not to work with port numbers (e.g. when developing local)
    $url = preg_replace('/:[0-9]+/', '', $url);
    return file_get_contents($url);
}

/**
 * loads template either from theme directory (first choice)
 * or this plugins directory (second choice)
 *
 * @deprecated
 *
 * @param string $path_to_template
 * @param array  $data
 * @param bool   $admin
 */
function wpva_render_template($path_to_template, $data = array(), $admin = false) {
    trigger_error('Method ' . __METHOD__ . ' is deprecated', E_USER_DEPRECATED);

    $template_path = locate_template(basename($path_to_template));
    if (!$template_path || $admin) {
        $template_path = __DIR__ . '/../templates/' . $path_to_template;
    }

    $template_content = '';
    if (is_file($template_path)) {
        extract($data);
        ob_start();
        /** @noinspection PhpIncludeInspection */
        require($template_path);
        $template_content = ob_get_clean();
    }

    echo $template_content;
}