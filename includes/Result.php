<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_Result')) {

    class WPVA_Result extends WPVA_Base {

        /** @var WPVA_DataType_Vote */
        protected $vote;
        protected $parties = array();

        protected function handle_actions() {
            add_action('rest_api_init', function() {
                register_rest_route('wp/v2', '/wpva-voteadviser/(?P<id>\d+)/result', array(
                        'methods' => WP_REST_Server::READABLE,
                        'callback' => array('WPVA_Result', 'get_result_rest')
                    )
                );
            });
        }

        public function set_vote(WPVA_DataType_Vote $vote) {
            $this->vote = $vote;
            return $this;
        }

        public function set_parties(array $parties) {
            $this->parties = $parties;
            return $this;
        }

        public function render_voteadviser_result() {
            $result = array();
            foreach ($this->parties as $party) {
                $result[$party['ID']] = $party;
                $result[$party['ID']]['match_quote'] = 0;
            }
            $favorites = array_filter($this->vote->get_favorites());
            $maximum = 2*(count($this->vote->get_opinions()) + count($favorites));
            foreach ($this->vote->get_opinions() as $question_id => $opinion) {
                if (!$opinion) {
                    $maximum -= 2;
                    continue;
                }
                foreach ($this->parties as $party) {
                    $coefficient = 1;
                    if (isset($favorites[$question_id])) {
                        $coefficient = 2;
                    }
                    $answer = new WPVA_DataType_Answer();
                    $answer->fill_by_id(WPVA_DataType_Answer::get_id_by_question_and_party(
                        $question_id,
                        $party['ID']
                    ));
                    $result[$party['ID']]['match_quote'] += self::compareOpinions(
                        $opinion,
                        $answer->get_opinion()
                    ) * $coefficient;
                }
            }
            foreach ($this->parties as $party) {
                $quote = 1 - ($result[$party['ID']]['match_quote']/$maximum);
                $result[$party['ID']]['match_quote'] = round($quote * 100);
            }
            uasort($result, array('WPVA_Result', 'sort_result_by_match_quote'));
            wpva_render_template('voteadviser_result.php', array(
                'parties' => $result,
                'vote_id' => $this->vote->get_ID()
            ));
        }

        private static function compareOpinions($opinion1, $opinion2) {
            $opinions = array_unique(array_merge(array($opinion1), array($opinion2)));
            if (count($opinions) < 2) {
                return 0;
            }
            if (in_array('neutral', $opinions, true)) {
                return 1;
            }
            return 2;
        }

        private static function sort_result_by_match_quote($a, $b) {
            $comparison = $b['match_quote'] - $a['match_quote'];
            if ($comparison == 0) {
                $comparison = strcmp($a['post_title'], $b['post_title']);
            }
            return $comparison;
        }

        private static function sort_result_by_title($a, $b) {
            return strcmp($a['post_title'], $b['post_title']);
        }

        public function render_voteadviser_comparison() {
            $question_ids = array_keys($this->vote->get_opinions());
            $party_ids = array_column($this->parties, 'ID');
            $parties = WPVA_DataType_Party::get_parties($party_ids);
            uasort($parties, array('WPVA_Result', 'sort_result_by_title'));
            wpva_render_template('voteadviser_comparison.php', array(
                'questions' => WPVA_DataType_Question::get_questions(
                    $question_ids
                ),
                'answers' => WPVA_DataType_Answer::get_answers(
                    $question_ids,
                    $party_ids
                ),
                'parties' => $parties,
                'opinions' => $this->vote->get_opinions(),
                'favorites' => $this->vote->get_favorites(),
                'vote_id' => $this->vote->get_ID()
            ));
        }

        /**
         * @param WP_REST_Request $request
         * @return WP_REST_Response
         */
        public static function get_result_rest($request) {
            $parameters = $request->get_params();
            WPVA_Result::get_result($parameters);
            return new WP_REST_Response($parameters);
        }

        private static function get_result(array $params) {
            $parties = WPVA_DataType_VotingAdviser::get_parties($params['id']);
            $questions = WPVA_DataType_VotingAdviser::get_questions($params['id']);
            $result = array();
            foreach ($parties as $party) {
                $result[$party['ID']] = $party;
                $result[$party['ID']]['match_quote'] = 0;
            }
            $favorites = explode(',', $params['f']);
            $pro = explode(',', $params['p']);
            $neutral = explode(',', $params['n']);
            $contra = explode(',', $params['c']);
            $maximum = 2*(count($favorites) + count($pro) + count($neutral) + count($contra));
        }
    }
}