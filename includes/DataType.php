<?php
/**
 * author: rene burghardt
 */

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WPVA_DataType')) {

    /**
     * basic class for plugin data types
     *
     * @property int ID
     * @property string title
     * @property string content
     */
    abstract class WPVA_DataType extends WPVA_Base {

        /**
         * gets data for data type from db by id
         *
         * @param int $id
         * @return $this
         */
        public function fill_by_id($id) {
            $this->ID = trim($id);
            $post = get_post($this->ID);
            $this->title = $post->post_title;
            $this->content = $post->post_content;
            return $this;
        }

        /**
         * getter for external use
         *
         * @return array
         */
        public function get_data() {
            return $this->prepared_data(true);
        }

        /**
         * preparation of data for class internal use
         *
         * @param bool $force_id
         * @return array
         */
        protected function prepared_data($force_id = false) {
            if ($force_id || $this->has_ID()) {
                $data['ID'] = $this->get_ID();
            }
            $data['post_title'] = $this->get_title();
            $data['post_content'] = $this->get_content();
            /** @noinspection PhpUndefinedClassConstantInspection */
            $data['post_type'] = static::SLUG;
            return $data;
        }

        /**
         * checks whether an ID is set or not
         *
         * @return bool
         */
        public function has_ID() {
            if (isset($this->ID) && $this->ID) {
                return true;
            }
            return false;
        }

        /**
         * gets the ID. May saving data type to db.
         *
         * @return int
         */
        public function get_ID() {
            if (!$this->has_ID()) {
                // if there is no ID set, save data type to db to get an ID
                $id = $this->insert();
                if ($id <= 0) {
                    //TODO: error handling
                }
                $this->ID = $id;
            }
            return $this->ID;
        }

        /**
         * @return string
         */
        public function get_title() {
            return $this->title;
        }

        /**
         * @param string $title
         * @return $this
         */
        public function set_title($title) {
            $this->title = $title;
            return $this;
        }

        /**
         * @return string
         */
        public function get_content() {
            return $this->content;
        }

        /**
         * @param string $content
         * @return $this
         */
        public function set_content($content) {
            $this->content = $content;
            return $this;
        }

        /**
         * is either inserting or updating data type to db
         *
         * @return int $post_id by success, 0 by failure
         */
        public function save() {
            if ($this->has_ID()) {
                return $this->update();
            }
            return $this->insert();
        }

        /**
         * inserts data type to db
         *
         * @return int $post_id by success, 0 by failure
         */
        public function insert() {
            $data = $this->prepared_data(false);
            return wp_insert_post($data);
        }

        /**
         * updates data type in db
         *
         * @return int $post_id by success, 0 by failure
         */
        public function update() {
            $data = $this->prepared_data();
            if (isset($data['ID'])) {
                return wp_update_post($data);
            }
            return 0;
        }
    }
}