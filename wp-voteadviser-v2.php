<?php
/**
 * Plugin Name: WP-VoteAdviser
 * Text Domain: wp-voteadviser-v2
 * Domain Path: /lang
 * Description: A plugin, to create and display a voteadviser on your website.
 * Author:      René Burghardt
 * Author URI:  http://reneburghardt.de/
 * License:     GPL2
 * Version:     2.0
 */

/**
Copyright 2018 René Burghardt (email: me@reneburghardt.de)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// TODO: copy of license

// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

require_once('includes/utils.php');

$wpva_initiator = new WPVA_Initiator();
register_activation_hook(__FILE__, array($wpva_initiator, 'activate'));
register_deactivation_hook(__FILE__, array($wpva_initiator, 'deactivate'));