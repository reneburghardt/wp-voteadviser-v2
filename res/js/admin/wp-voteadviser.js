jQuery(document).ready(function() {
    jQuery("input[type='radio']").click(function () {
        var previousValue = jQuery(this).attr('previousValue');
        var name = jQuery(this).attr('name');

        if (previousValue == 'checked') {
            jQuery(this).removeAttr('checked');
            jQuery(this).attr('previousValue', false);
        }
        else {
            jQuery("input[name='" + name + "']:radio").attr('previousValue', false);
            jQuery(this).attr('previousValue', 'checked');
        }
    });

    jQuery(".question.closed").click(function(){
        var classList = jQuery(this).attr('class').split(/\s+/);
        jQuery('.'+classList[1]).slideDown("fast");
        jQuery(this).removeClass("closed").addClass("open");
    });

    jQuery(".party-list-remove").click(function(){
        if (confirm('Are you sure to delete this?')) {
            jQuery(this).parent().hide();
            jQuery(this).find('input[type="checkbox"]').prop('checked', false);
        }
        return false;
    });

    jQuery('.metabox_submit').click(function(e) {
        e.preventDefault();
        jQuery('#publish').click();
    });
});