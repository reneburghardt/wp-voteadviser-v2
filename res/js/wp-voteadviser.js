jQuery(document).ready(function($){

    $(".voteadviser").each(function() {
        var id = $(this).attr('id').split('-')[1];
        setup_questions(id);
    });

    // TODO extract duplicate code of jquery ajax requesting
    function setup_questions(id) {
        $.ajax({
            type: "GET",
            url: wpRestApiUrl + "wp/v2/wpva-voteadviser/" + id + "/questions",
            dataType: 'text',
            success: function(content) {
                setup_voteadviser(id, JSON.parse(content));
            },
            error: function(request, status, error) {
                console.log(request.responseText);
            }
        });
    }

    function setup_voteadviser(id, questions) {
        $.ajax({
            type: "GET",
            url: wpRestApiUrl + "wp/v2/wpva-voteadviser/" + id,
            dataType: 'text',
            success: function(content) {
                build_voteadviser(JSON.parse(content), questions);
            },
            error: function(request, status, error) {
                console.log(request.responseText);
            }
        });
    }

    function build_voteadviser(voteadviser, questions) {
        Vue.component('questions', {
            props: ['id', 'title', 'question', 'nr', 'count'],
            template: '#question-template'
        });
        new Vue({
            el: $('#voteadviser-' + voteadviser.id)[0],
            data: {
                title: voteadviser.title.rendered,
                questions: questions
            },
            mounted: function() {
                $(".voteadviser-form").submit(function(e){
                    e.preventDefault();
                    evaluate_voteadviser(voteadviser.id);
                });
            }
        });
    }

    function evaluate_voteadviser(id) {
        var entry = {f: [], p: [], n: [], c: []};

        $('.voteadviser-form .voteadviser-favorite').each(function() {
            if ($(this).is(':checked')) {
                entry.f.push($(this).val());
            }
        });

        $('.voteadviser-form .voteadviser-pro-opinion').each(function() {
            if ($(this).is(':checked')) {
                entry.p.push($(this).val());
            }
        });

        $('.voteadviser-form .voteadviser-neutral-opinion').each(function() {
            if ($(this).is(':checked')) {
                entry.n.push($(this).val());
            }
        });

        $('.voteadviser-form .voteadviser-contra-opinion').each(function() {
            if ($(this).is(':checked')) {
                entry.c.push($(this).val());
            }
        });

        request_result(id, entry);
    }

    function request_result(id, entry) {
        $.ajax({
            type: "GET",
            url: wpRestApiUrl + "wp/v2/wpva-voteadviser/" + id + "/result?f=" + entry.f.join() + "&p=" + entry.p.join() + "&n=" + entry.n.join() + "&c=" + entry.c.join(),
            dataType: 'text',
            success: function(content) {
                console.log(content);
            },
            error: function(request, status, error) {
                console.log(request.responseText);
            }
        });
    }

});