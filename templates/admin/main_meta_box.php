<?php
    // Exit when accessed directly.
    if (!defined('ABSPATH')) {
        exit;
    }
?>

<input title="hidden old question ids" type="hidden" value="<?= $question_ids; ?>" name="_wpva_old_question_ids"/>
<input title="hidden old party ids" type="hidden" value="<?= $party_ids; ?>" name="_wpva_old_party_ids"/>
<?php
    if (count($parties) < 1) {
        ?>
        <?=__('Please add a party at first.', 'wp-voteadviser');?>
        <?php
    }
    else {
?>
    <table class="voteadviser-wrapper">
    <?php
    $count = count($questions);
    $inc = 0;
    foreach ($questions as $nr => $question) {
    ?>
         <tr class="question-wrapper">
             <td>
                 <span><?=($nr+1);?> / <?=$count;?></span>
                 <button type="submit" name="wpva_question_remove" class="wpva_question_remove button button-small" value="<?=$question['ID'];?>">
                     <i class="material-icons">remove</i>
                 </button>
                 <?php if($nr > 0) { ?>
                 <button type="submit" name="wpva_question_up" class="wpva_question_up button button-small" value="<?=$question['ID'];?>">
                     <i class="material-icons">arrow_upward</i>
                 </button>
                 <?php } ?>
             </td>
             <td class="question question-<?=$question['ID'];?> closed" colspan="2">
                 <i class="material-icons arrow-down float-right">keyboard_arrow_down</i>
                 <i class="material-icons arrow-remove float-right">remove</i>
                 <b><?=$question['post_title'];?></b>
                 <br/>
                 <?=$question['post_content'];?>
             </td>
         </tr>
    <?php
        foreach ($parties as $party) {
            $opinion = $answers[$question['ID']][$party['ID']]['opinion'];
            $name = "[{$question['ID']}][{$party['ID']}]";
            $inc++;
        ?>
            <tr class="party-wrapper question-<?=$question['ID'];?>">
                <td rowspan="2" class="border-bottom">
                    <?php if (isset($party['logo_url']) && !empty($party['logo_url'])) { ?>
                    <img src="<?=$party['logo_url'];?>" width="100px" height="100px"/>
                    <?php } else { ?>
                    <?=$party['post_title'];?>
                    <?php } ?>
                </td>
                <td>
                    <textarea
                        placeholder="<?=__('Enter answer here', 'wp-voteadviser');?>"
                        name="answers<?=$name;?>"
                        title="Answer of <?=$party['post_title'];?> for <?=$question['post_title'];?>"
                    ><?=$answers[$question['ID']][$party['ID']]['post_content'];?></textarea>
                </td>
            </tr>
            <tr class="opinion-wrapper question-<?=$question['ID'];?>">
                <td class="border-bottom">
                    <input type="radio" name="opinions<?=$name;?>" id="pro_opinion_checkbox_<?=$inc;?>" value="pro" <?=$opinion === 'pro' ? 'checked' : '';?>/>
                    <label class="pro_opinion" for="pro_opinion_checkbox_<?=$inc;?>">
                        <i class="material-icons opinion-unchecked">check_box_outline_blank</i>
                        <i class="material-icons opinion-checked">check_box</i>
                        <?=__('Approve', 'wp-voteadviser');?>
                    </label>
                    <input type="radio" name="opinions<?=$name;?>" id="neutral_opinion_checkbox_<?=$inc;?>" value="neutral" <?=$opinion === 'neutral' ? 'checked' : '';?>/>
                    <label class="neutral_opinion" for="neutral_opinion_checkbox_<?=$inc;?>">
                        <i class="material-icons opinion-unchecked">check_box_outline_blank</i>
                        <i class="material-icons opinion-checked">check_box</i>
                        <?=__('Neutral', 'wp-voteadviser');?>
                    </label>
                    <input type="radio" name="opinions<?=$name;?>" id="contra_opinion_checkbox_<?=$inc;?>" value="contra" <?=$opinion === 'contra' ? 'checked' : '';?>/>
                    <label class="contra_opinion" for="contra_opinion_checkbox_<?=$inc;?>">
                        <i class="material-icons opinion-unchecked">check_box_outline_blank</i>
                        <i class="material-icons opinion-checked">check_box</i>
                        <?=__('Decline', 'wp-voteadviser');?>
                    </label>
                </td>
            </tr>
        <?php
        }
    }
    ?>
        <tr class="question-wrapper">
            <td></td>
            <td colspan="2" class="question question-new closed">
                <i class="material-icons arrow-down float-right">keyboard_arrow_down</i>
                <i class="material-icons arrow-remove float-right">remove</i>
                <b><?=__('Add new question', 'wp-voteadviser');?></b>
                <div class="new-question-wrapper question-new">
                    <input name="question_title_new" placeholder="<?=__('Enter new question title', 'wp-voteadviser');?>"/>
                    <br/>
                    <textarea
                        placeholder="<?=__('Enter new question or leave blank for canceling', 'wp-voteadviser');?>"
                        name="question_new" title="New question"
                    ></textarea>
                </div>
            </td>
        </tr>
        <?php
        foreach ($parties as $party) {
            $name = "[new][{$party['ID']}]";
            $inc++;
            ?>
            <tr class="party-wrapper question-new">
                <td rowspan="2" class="border-bottom">
                    <?php if (isset($party['logo_url']) && !empty($party['logo_url'])) { ?>
                    <img src="<?=$party['logo_url'];?>" width="100px" height="100px"/>
                    <?php } else { ?>
                    <?=$party['post_title'];?>
                    <?php } ?>
                </td>
                <td>
                    <textarea
                        placeholder="<?=__('Enter answer here', 'wp-voteadviser');?>"
                        name="answers<?=$name;?>"
                        title="Answer of <?=$party['post_title'];?>"
                    ></textarea>
                </td>
            </tr>
            <tr class="opinion-wrapper question-new">
                <td class="border-bottom">
                    <input type="radio" name="opinions<?=$name;?>" id="pro_opinion_checkbox_<?=$inc;?>" value="pro" />
                    <label class="pro_opinion" for="pro_opinion_checkbox_<?=$inc;?>">
                        <i class="material-icons opinion-unchecked">check_box_outline_blank</i>
                        <i class="material-icons opinion-checked">check_box</i>
                        <?=__('Approve', 'wp-voteadviser');?>
                    </label>
                    <input type="radio" name="opinions<?=$name;?>" id="neutral_opinion_checkbox_<?=$inc;?>" value="neutral" />
                    <label class="neutral_opinion" for="neutral_opinion_checkbox_<?=$inc;?>">
                        <i class="material-icons opinion-unchecked">check_box_outline_blank</i>
                        <i class="material-icons opinion-checked">check_box</i>
                        <?=__('Neutral', 'wp-voteadviser');?>
                    </label>
                    <input type="radio" name="opinions<?=$name;?>" id="contra_opinion_checkbox_<?=$inc;?>" value="contra" />
                    <label class="contra_opinion" for="contra_opinion_checkbox_<?=$inc;?>">
                        <i class="material-icons opinion-unchecked">check_box_outline_blank</i>
                        <i class="material-icons opinion-checked">check_box</i>
                        <?=__('Decline', 'wp-voteadviser');?>
                    </label>
                </td>
            </tr>
        <?php
        }
        ?>
        <tr class="opinion-wrapper question-new">
            <td colspan="2">
                <input type="submit" name="wpva_add_question" id="wpva_add_question" class="button button-small metabox_submit" value="<?=__('Add question', 'wp-voteadviser');?>">
            </td>
        </tr>
    </table>
<?php } ?>