<?php
// Exit when accessed directly.
if (!defined('ABSPATH')) {
    exit;
}
?>
<div>
    <?=__('Add new party', 'wp-voteadviser');?>:
    <?php if (count($all_parties) > 0) { ?>
    <select name="_wpva_party_ids[]">
        <option value="-1"><?=__('Please choose a party', 'wp-voteadviser');?></option>
        <?php
        $party_ids = array_column($parties, 'ID');
        foreach ($all_parties as $party) {
            if (!in_array($party->ID, $party_ids)) {
                ?>
                <option value="<?=$party->ID;?>"><?=$party->post_title;?></option>
                <?php
            }
        }
        ?>
    </select>
    <input type="submit" name="wpva_add_party" id="wpva_add_party" class="button button-small metabox_submit" value="<?=__('Add', 'wp-voteadviser');?>">
    <?php } else { ?>
    <i><?='-- ' . __('no party left to add', 'wp-voteadviser') . ' --';?></i>
    <?php } ?>
</div>

<div class="party-list">
    <?php
        foreach ($parties as $party) {
            ?>
            <div>
                <img src="<?=$party['logo_url'];?>" width="100px" height="100px"/>
                <br/>
                <label class="party-list-remove">
                    <?=__('Remove', 'wp-voteadviser');?>
                    <input type="checkbox" name="_wpva_party_ids[]" value="<?=$party['ID'];?>" checked>
                </label>
            </div>
            <?php
        }
    ?>
</div>